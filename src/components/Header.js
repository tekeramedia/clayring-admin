import React from 'react'
import Logo from '../assets/footerLogo.svg'

const Header = () => {
    return (
        <header>
            <a href="/"><img src={Logo} alt="logo" /></a>
            <a href='https://clayring-website-4na224n8d-tekeramedia.vercel.app/' style={{ color: '#fff' }}>Back to site</a>
        </header>
    )
}

export default Header
