import React from 'react'
import icon from '../assets/iconi.svg'
import home from '../assets/home.svg'
import refresh from '../assets/refresh.svg'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Avatar from '@material-ui/core/Avatar'
import { deepOrange, deepPurple } from '@material-ui/core/colors'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    orange: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
    },
}))



const SideNav = () => {
    const classes = useStyles()
    return (
        <aside>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
            }}>
                <Link to="/site"><img src={icon} alt='icon' /></Link>
                <Link to="/"><img src={home} alt='home' /></Link>
                <Link to="/"><img src={refresh} alt='refresh' /></Link>
            </div>
            <Avatar className={classes.orange} style={{ justifySelf: 'end' }}>A</Avatar>
        </aside >
    )
}

export default SideNav
