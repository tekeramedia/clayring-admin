import React, { useState, useContext, useEffect } from 'react'
import './App.scss'
import Login from './pages/Login'
import Dashboard from './pages/Dashboard'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import AuthApi from "./AuthContext"
import Cookie from 'js-cookie'
import ProtectedDashboard from './protectedRoutes/DashboardRoute'
import ProtectedLogin from './protectedRoutes/LoginRoute'


function App() {

  const [auth, setAuth] = useState(false)

  const authorizeUser = () => {
    const user = Cookie.get('adminAccessToken')
    user && setAuth(true)
  }

  useEffect(() => {
    authorizeUser()
  }, [])

  return (
    <AuthApi.Provider value={{ auth, setAuth }}>
      <Router>
        <Routes />
      </Router>
    </AuthApi.Provider>
  )
}

const Routes = () => {
  const AuthContext = useContext(AuthApi)
  return (
    <Switch>
      <ProtectedLogin exact path="/" component={Login} auth={AuthContext.auth} />
      <ProtectedDashboard path="/dashboard" component={Dashboard} auth={AuthContext.auth} />
    </Switch>
  )
}


export default App
