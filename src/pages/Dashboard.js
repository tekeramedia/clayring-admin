import React, { useState, useEffect, useContext, forwardRef } from 'react'
import SideNav from '../components/SideNav'
import { DataGrid } from '@material-ui/data-grid'
import axios from 'axios'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Cookie from 'js-cookie'
import AuthApi from '../AuthContext'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Modal from '@material-ui/core/Modal'
import Backdrop from '@material-ui/core/Backdrop'
import { useSpring, animated } from '@react-spring/web'
import Button from '../components/Button'

const columns = [
    { field: 'id', headerName: 'ID', width: 40 },
    {
        field: '_id',
        headerName: 'ID',
        width: 400,
        editable: false,
    },
    {
        field: 'fullname',
        headerName: 'Full name',
        width: 420,
        editable: false,
    },
    {
        field: 'email',
        headerName: 'Email',
        description: 'This column has a value getter and is not sortable.',
        sortable: false,
        width: 300,
    }
]

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}))

const Fade = forwardRef(function Fade(props, ref) {
    const { in: open, children, onEnter, onExited, ...other } = props;
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: open ? 1 : 0 },
        onStart: () => {
            if (open && onEnter) {
                onEnter()
            }
        },
        onRest: () => {
            if (!open && onExited) {
                onExited();
            }
        },
    })

    return (
        <animated.div ref={ref} style={style} {...other}>
            {children}
        </animated.div>
    )
})

Fade.propTypes = {
    children: PropTypes.element,
    in: PropTypes.bool.isRequired,
    onEnter: PropTypes.func,
    onExited: PropTypes.func,
}

const Dashboard = () => {

    const classes = useStyles()

    const [data, setData] = useState([])
    const [selectionModel, setSelectionModel] = useState([])
    const [customerID, setCustomerID] = useState()
    const [open, setOpen] = useState(false)

    // form states
    const [total, setTotal] = useState()
    const [paid, setPaid] = useState()
    const [balance, setBalance] = useState()
    const [nextPaymnet, setNextPayment] = useState()
    const [span, setSpan] = useState()

    const handleOpen = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

    const Auth = useContext(AuthApi)


    useEffect(() => {
        async function fetchData() {
            const response = await axios.get('/api/v1/customers')
            const data = await response.data
            setData(data)
        }
        fetchData()
    }, [])

    const rows = data

    const handleLogout = async () => {
        const adminRefreshToken = Cookie.get('adminRefreshToken')
        const adminAccessToken = Cookie.get('adminAccessToken')

        try {
            await axios.post('/admin/auth/signout', { token: adminRefreshToken }, {
                headers: {
                    "token": `Bearer ${adminAccessToken}`
                }
            })

            Auth.setAuth(false)
            Cookie.remove('adminAccessToken')
            Cookie.remove('adminRefreshToken')

        } catch (err) {
            console.log('error logging out')
        }
    }

    return (
        <section className='dashboard'>
            <div>
                <SideNav />
            </div>

            <section className='dash'>
                <div className='top'>
                    <button className="verified">
                        <i className="fa fa-check" style={{ color: '#1dbf73', padding: '0 .3em' }}></i>
                        <span style={{ fontSize: '14px' }}>Verified Admin</span>
                    </button>

                    <button
                        style={{
                            width: '131px',
                            height: '40px',
                            background: "#fff3f3",
                            color: "#fc4747",
                            fontSize: '16px',
                            cursor: 'pointer',
                            border: 'none',
                            borderRadius: '5px'
                        }}

                        onClick={handleLogout}
                    >
                        Logout
                    </button>

                </div>
                <React.Fragment>
                    <Grid container spacing={3} style={{ marginBottom: '2em', width: '95%', alignItems: 'center', padding: '0 1em' }}>
                        <Grid item xs={12} lg={4} xl={4} md={4}>
                            <Paper style={{ padding: '1em', display: 'flex', alignItems: 'flex-start', flexDirection: 'column', height: '100px' }}>
                                <small>Registered Customers</small>
                                <h1 style={{ fontSize: '36px' }}>{data.length}</h1>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} lg={4} xl={4} md={4}>
                            <Paper style={{ padding: '1em', display: 'flex', alignItems: 'flex-start', flexDirection: 'column', height: '100px' }}>
                                <small>Applications</small>
                                <h1 style={{ fontSize: '36px' }}>{customerID}</h1>
                            </Paper>
                        </Grid>
                        <Grid item xs={12} lg={4} xl={4} md={4}>
                            <Paper style={{ padding: '1em', display: 'flex', alignItems: 'flex-start', height: '100px' }}>
                                <small>Balance</small>
                            </Paper>
                        </Grid>
                    </Grid >
                </React.Fragment >

                <Modal
                    aria-labelledby="spring-modal-title"
                    aria-describedby="spring-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={open}>
                        <div className='form-modal' style={{ width: '500px' }}>
                            <form onSubmit>
                                <div style={{ display: 'flex' }}>
                                    <div className="form-group">
                                        <label htmlFor="">Total</label>
                                        <input type="number" value={total} onChange={(e) => setTotal(e.target.value)} required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="">Paid</label>
                                        <input type="number" value={paid} onChange={(e) => setPaid(e.target.value)} required />
                                    </div>
                                </div>
                                <div style={{ display: 'flex' }}>
                                    <div className="form-group">
                                        <label htmlFor="">Balance</label>
                                        <input type="number" value={balance} onChange={(e) => setBalance(e.target.value)} required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="">Next Payment</label>
                                        <input type="date" value={nextPaymnet} onChange={(e) => setNextPayment(e.target.value)} required />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Span (months)</label>
                                    <input type="number" value={span} onChange={(e) => setSpan(e.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <Button caption="Update Payment Info" type="submit" width="inherit" />
                                </div>
                            </form>
                        </div>
                    </Fade>
                </Modal>
                <div style={{ height: 500, width: '100%' }}>
                    <DataGrid
                        rows={rows}
                        disableDensitySelector
                        columns={columns}
                        pageSize={6}
                        checkboxSelection
                        disableSelectionOnClick
                        selectionModel={selectionModel}
                        onSelectionModelChange={(selection) => {
                            setSelectionModel(selection)
                            const newSelectionModel = selection
                            if (newSelectionModel.length > 1) {
                                const selectionSet = new Set(selectionModel)
                                const result = newSelectionModel.filter(
                                    (s) => !selectionSet.has(s)
                                )
                                setSelectionModel(result)
                            }
                            else {
                                setSelectionModel(newSelectionModel)
                            }
                        }}
                        onRowClick={(params, event) => {
                            if (!event.ignore) {
                                setCustomerID(params.row.id)
                                handleOpen()
                            }
                        }}
                    />
                </div>
            </section>
        </section >
    )
}

export default Dashboard
