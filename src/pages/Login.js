import React, { useState, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import axios from "axios"
import Cookie from "js-cookie"
import AuthApi from "../AuthContext"
import Header from "../components/Header"
import "../App.scss"
import Button from '../components/Button'
import { makeStyles } from '@material-ui/core/styles'
import Alert from '@material-ui/lab/Alert'

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}))


const Login = () => {

    const classes = useStyles()

    const Auth = useContext(AuthApi)
    const history = useHistory()

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)


    const handleLogin = async (e) => {
        e.preventDefault()
        setError(false)
        try {
            const response = await axios.post('/admin/auth/signin', { email, password })
            const { accessToken, refreshToken } = response.data
            Auth.setAuth(true)
            Cookie.set('adminAccessToken', accessToken)
            Cookie.set('adminRefreshToken', refreshToken)
            setSuccess(true)
        } catch (err) {
            setError(true)
            setTimeout(() => {
                history.push('/')
            }, 2000)
        }
    }

    return (
        <>
            <Header />

            <div class="form-modal">
                {error ?
                    <div className={classes.root}>
                        <Alert severity="error">Error Logging in!</Alert>
                    </div>
                    : null
                }
                <br />
                {success ?
                    <div className={classes.root}>
                        <Alert severity="success">Login Successful</Alert>
                    </div>
                    : null}
                <br />
                <i class="fa fa-user-circle-o" aria-hidden="true" style={{ fontSize: '50px', textAlign: 'center' }}></i>
                <form onSubmit={handleLogin}>
                    <div className="form-group">
                        <label htmlFor="">Email</label>
                        <input type="email" placeholder="admin@clayring.com" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Password</label>
                        <input type="password" placeholder="*******" value={password} onChange={(e) => setPassword(e.target.value)} required />

                        <small><a href="/">Forgot password?</a></small>
                    </div>
                    <div className="form-group">
                        <Button caption="Login" type="submit" width="inherit" />
                    </div>
                </form>
            </div>


        </>
    )
}

export default Login
