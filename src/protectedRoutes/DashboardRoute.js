import { Route, Redirect } from "react-router-dom"

const ProtectedDashboard = ({ auth, component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={
                () => auth ? (
                    < Component />
                ) : (
                    <Redirect to="/" />
                )
            }
        />
    )
}

export default ProtectedDashboard
